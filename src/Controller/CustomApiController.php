<?php

namespace App\Controller;

use App\Entity\Eleve;
use App\Repository\EleveRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Routing\Annotation\Route;

class CustomApiController extends AbstractController
{

    public function calculMoyenne(Eleve $eleve)
    {
        $moyenne = 0;

        if($eleve->getListeNotes() != null && count($eleve->getListeNotes()) > 0)
        {
            foreach ($eleve->getListeNotes() as $note)
            {
                $moyenne += $note->getValeur();
            }
            $moyenne /= count($eleve->getListeNotes());
        }

        return $moyenne;
    }


    /**
     * @param Request $request
     * @param Eleve $eleve
     * @return JsonResponse
     * @Route("/api/moyenne/eleve/{id}", name="moyenne_api")
     */
    public function moyenneEleve(Request $request, $id, EleveRepository $eleveRepository)
    {
        $eleve = $eleveRepository->find($id);
        if($eleve == null)
            throw new NotFoundHttpException("Aucun étudiant n'existe avec l'identifiant fourni");

        $moyenneEleve = $this->calculMoyenne($eleve);

        return new JsonResponse([
            'moyenneEleve' => $moyenneEleve
         ]);
    }

    /**
     * @param Request $request
     * @return JsonResponse
     * @Route("/api/moyenne/classe/", name="moyenne_classe_api")
     */
    public function moyenneClasse(Request $request, EleveRepository $eleveRepository)
    {
        $moyenne = 0;
        $listeEleves = $eleveRepository->findAll();
        $divider=0;

        if($listeEleves != null)
        {
            foreach ($listeEleves as $eleve){
                $moyenne += $this->calculMoyenne($eleve);
                $divider++;
            }

            $moyenne /= $divider;
        }

        return new JsonResponse([
            'moyenneClasse' => $moyenne
        ]);
    }
}
