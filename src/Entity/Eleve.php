<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\EleveRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"reader"}},
 *     denormalizationContext={"groups"={"writer"}}
 * )
 * @ORM\Entity(repositoryClass=EleveRepository::class)
 */
class Eleve
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"reader"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=50)
     * @Groups({"reader", "writer"})
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=100)
     * @Groups({"reader", "writer"})
     */
    private $prenoms;

    /**
     * @ORM\Column(type="date")
     * @Groups({"reader", "writer"})
     */
    private $birthday;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="App\Entity\Note", mappedBy="eleve")
     * @Groups({"reader"})
     */
    private $listeNotes;

    public function __construct()
    {
        $this->listeNotes = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = $nom;

        return $this;
    }

    public function getPrenoms(): ?string
    {
        return $this->prenoms;
    }

    public function setPrenoms(string $prenoms): self
    {
        $this->prenoms = $prenoms;

        return $this;
    }

    public function getBirthday(): ?\DateTimeInterface
    {
        return $this->birthday;
    }

    public function setBirthday(\DateTimeInterface $birthday): self
    {
        $this->birthday = $birthday;

        return $this;
    }

    /**
     * @return Collection|Note[]
     */
    public function getListeNotes(): Collection
    {
        return $this->listeNotes;
    }

    public function addListeNote(Note $listeNote): self
    {
        if (!$this->listeNotes->contains($listeNote)) {
            $this->listeNotes[] = $listeNote;
            $listeNote->setEleve($this);
        }

        return $this;
    }

    public function removeListeNote(Note $listeNote): self
    {
        if ($this->listeNotes->contains($listeNote)) {
            $this->listeNotes->removeElement($listeNote);
            // set the owning side to null (unless already changed)
            if ($listeNote->getEleve() === $this) {
                $listeNote->setEleve(null);
            }
        }

        return $this;
    }
}
