<?php

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use App\Repository\NoteRepository;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

/**
 * @ApiResource(
 *     normalizationContext={"groups"={"noteReader"}},
 *     denormalizationContext={"groups"={"noteWriter"}}
 * )
 * @ORM\Entity(repositoryClass=NoteRepository::class)
 */
class Note
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     * @Groups({"noteReader","reader"})
     */
    private $id;

    /**
     * @ORM\Column(type="float")
     * @Groups({"noteReader","reader", "noteWriter"})
     */
    private $valeur;

    /**
     * @ORM\Column(type="string", length=255)
     * @Groups({"noteReader","reader", "noteWriter"})
     */
    private $matiere;

    /**
     * @var Eleve
     * @ORM\ManyToOne(targetEntity="App\Entity\Eleve", inversedBy="listeNotes")
     * @Groups({"noteReader","noteWriter"})
     */
    private $eleve;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getValeur(): ?float
    {
        return $this->valeur;
    }

    public function setValeur(float $valeur): self
    {
        $this->valeur = $valeur;

        return $this;
    }

    public function getMatiere(): ?string
    {
        return $this->matiere;
    }

    public function setMatiere(string $matiere): self
    {
        $this->matiere = $matiere;

        return $this;
    }

    public function getEleve(): ?Eleve
    {
        return $this->eleve;
    }

    public function setEleve(?Eleve $eleve): self
    {
        $this->eleve = $eleve;

        return $this;
    }
}
